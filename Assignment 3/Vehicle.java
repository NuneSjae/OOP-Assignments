
/**
 * =
 * 
 * @author (Nun Srijae) 
 * @version (26/11/2016)
 */
public class Vehicle
{
    // A unique ID for vehicle
    private String ident;
    // The destination of vehicle.
    private String destination;
    // The location of vehicle.
    private String location;
    /**
     * Constructor for objects of class Vehicle
     * @param base of the vehicle
     * @param id of the vehicle
     */
    public Vehicle(String base, String id)
    {
        ident = id;
        this.destination = null; 
        location = base;
    }
    /**
     * Another constructor for object of class Vehicle
     * @param id is created so that Shuttle class could access it 
     */
    public Vehicle(String id)
    {
        ident = id;
    }
    
    /**
     * Return the ID of the vehicle.
     * @return The ID of the vehicle.
     */
    protected String getIdent()
    {
        return ident;
    }

    /**
     * Return the destination of vehicle.
     * @return The destination of the vehicle.
     */
    protected String getDestination()
    {
        return destination;
    }

    /**
     * Set the intented destination of the vehicle.
     * @param destination The intended destination.
     */
    protected void setDestination(String destination)
    {
        this.destination = destination;
    }

    /**
     * Return the location of the vehicle.
     * @return The location of the vehicle.
     */
    protected String getLocation()
    {
        return location;
    }

    /**
     * Set the intented location of the vehicle.
     * @param destination The intended destination.
     */
    protected String setLocation(String location)
    {
        return this.location = location;
    }

    /**
     * Return the status of vehicle.
     * @return The status.
     */
    protected String getStatus()
    {
        if(destination != null)
        {
            return getIdent() + " at " + getLocation() + " headed for " +
            getDestination();

        }
        else
        {
            return getIdent() + " at " + getLocation() + " and is available";
         
        }
    }

}

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Optional;
import java.util.Random;
import java.util.Vector;

/**
 * The test class MonitorTest.
 *
 * @author David J. Barnes (d.j.barnes@kent.ac.uk)
 * @version 2016.11.02
 */
public class MonitorTest
{
    // The monitor to be tested.
    private Monitor monitor;
    // A generator of random values.
    private Random rand;
    // Shadow copy of the readings.
    private Vector<Reading> shadow;
    // The number of days for which there are readings.
    private int numDays;
    // The number of locations in which readings are taken.
    private int numLocations;
    
    /**
     * Constructor for test class MonitorTest
     */
    public MonitorTest()
    {
        rand = new Random();
        // The remaining fields will be initialised properly
        // in the setup method before each test.
        monitor = null;
        numDays = 0;
        numLocations = 0;
    }

    /**
     * Sets up the test fixture.
     * Generates random values for the readings.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        monitor = new Monitor();
        shadow = new Vector<>();
        numLocations = 4 + rand.nextInt(5);
        numDays = 7 + rand.nextInt(7);
        for(int day = 1; day <= numDays; day++) {
            for(int location = 1; location <= numLocations; location++) {
                double temperature = -rand.nextInt(10) + rand.nextInt(10) + 20 * rand.nextDouble();
                Reading r = new Reading(location, day, temperature);
                monitor.addReading(r);
                shadow.add(r);
            }
        }
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    /**
     * Test that the number of readings in the monitor is correct.
     */
    public void size()
    {
        assertEquals(numDays * numLocations, monitor.getNumberOfReadings());
    }

    @Test
    /**
     * Test that there are no readings for an invalid location.
     */
    public void sizeLocation()
    {
        assertEquals(0, monitor.getNumberOfReadings(0));
    }

    @Test
    /**
     * Test the number of readings for location 1.
     */
    public void sizeLocation1()
    {
        assertEquals(numDays, monitor.getNumberOfReadings(1));
    }

    @Test
    /**
     * Test the number of readings for location 7.
     */
    public void sizeLocation7()
    {
        assertEquals(numDays, monitor.getNumberOfReadings(1));
    }

    @Test
    /**
     * Test that removing of readings for an invalid day
     * works ok.
     */
    public void remove0()
    {
        assertEquals(numDays * numLocations, monitor.getNumberOfReadings());
        monitor.removeReadingsOnDay(0);
        assertEquals(numDays * numLocations, monitor.getNumberOfReadings());
    }

    @Test
    /**
     * Test removal of readings from day 5.
     */
    public void remove5()
    {
        assertEquals(numDays * numLocations, monitor.getNumberOfReadings());
        monitor.removeReadingsOnDay(5);
        assertEquals((numDays-1) * numLocations, monitor.getNumberOfReadings());
    }


    @Test
    /**
     * Test failure to find a reading when there are none.
     */
    public void ceilingFail()
    {
        monitor = new Monitor();
        assertEquals(-1, monitor.findFirstAbove(10));
    }

    @Test
    /**
     * Test finding a value when the ceiling is lower than all values.
     */
    public void ceilingMinus1()
    {
        ceilingCheck(-1);
    }

    @Test
    /**
     * Test finding a value when the ceiling is mid range.
     */
    public void ceiling18()
    {
        ceilingCheck(18);        
    }

    @Test
    /**
     * Test finding a value when the ceiling is higher than all values.
     */
    public void ceiling100()
    {
        ceilingCheck(100);
    }
    
    /**
     * Check the behavior when finding values above a given ceiling.
     * Run tests with both positive temperature values and negative ones.
     * @param ceiling The ceiling above which values are looked for.
     */
    private void ceilingCheck(int ceiling)
    {
        // Check first with positive temperatures, or a mix of
        // positive and negative.
        runCeilingTest(ceiling);
        // Check with wholly negative temperatures.
        setUpNegative();
        runCeilingTest(ceiling);
    }
    
    /**
     * Check the behavior when finding values above a given ceiling.
     * @param ceiling The ceiling above which values are looked for.
     */
    private void runCeilingTest(int ceiling)
    {
        int found = monitor.findFirstAbove(ceiling);
        if(shadow.isEmpty()) {
            // Nothing to find.
            assertEquals(-1, found);
        }
        else {
            // Find the maximum.
            double max = 
                    shadow.stream()
                          .mapToDouble(r -> r.getTemperature())
                          .max()
                          .getAsDouble();
            Optional<Reading> first;
            if(max > ceiling) {
                // One higher than the ceiling should have been found.
                first = 
                    shadow.stream()
                          .filter(r -> r.getTemperature() > ceiling)
                          .limit(1).reduce((r1, r2) -> r1);
            }
            else {
                // Find the first occurrence of max.
                first = 
                    shadow.stream()
                          .filter(r -> r.getTemperature() == max)
                          .limit(1).reduce((r1, r2) -> r1);
            }
            assertEquals(first.get().getDay(), found);
        }
                       
    }

    /**
     * Sets up the test fixture.
     * Generates random values for the readings.
     * All temperature readings are negative.
     *
     * Called before every test case method.
     */
    private void setUpNegative()
    {
        monitor = new Monitor();
        shadow = new Vector<>();
        numLocations = 4 + rand.nextInt(5);
        numDays = 7 + rand.nextInt(7);
        for(int day = 1; day <= numDays; day++) {
            for(int location = 1; location <= numLocations; location++) {
                // Force a negative value.
                double temperature = -rand.nextInt(30) - 1;
                Reading r = new Reading(location, day, temperature);
                monitor.addReading(r);
                shadow.add(r);
            }
        }
    }


}




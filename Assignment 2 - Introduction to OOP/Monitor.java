import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
/**
 * Monitor multiple sensor readings.
 * @author Nun Srijae
 * @version 14/11/16
 */
public class Monitor
{
    //creating an arraylist to store readings of the temperature
    public ArrayList<Reading> readings; 
    /**
     * Create a Monitor object.
     */
    public Monitor()
    {
        // initialising  a new array list in the constructor
        readings = new ArrayList<>();  
    }

    /**
     * Add details of a new reading to the collection.
     * @param readingDetails The details of the reading.
     */
    public void addReading(Reading reading)
    {
        readings.add(reading);  //adding @param reading into the collection
    }

    /**
     * An acessor method to get the number of readings.
     * @return the number of readings.
     */
    public int getNumberOfReadings()
    {
        return readings.size(); //return how many readings there are in the collection
    }

    /**
     * Get the number of readings in the given location.
     * @param location The location of the readings.
     * @return the number of readings in the given location.
     */
    public int getNumberOfReadings(int location)
    {
        int numberOfLoc = 0; //local variable and set its default value to 0
        for(Reading readings : readings)
        {
            if(readings.getLocation() == location)
            {
                numberOfLoc++; //if the statement above is true, increment numberOfLoc by 1
            }
        }
        return numberOfLoc; //return number of readings in @param location
    }

    /**
     * List all of the readings, one per line.
     */
    public void list()
    {
        System.out.println("Readings: "); //displaying "readings:" on the screen
        for(Reading readings : readings) 
        {  
            System.out.println(readings.getDetails());  //getting details of the readings
            //day and location
        }
        System.out.println(); 
    }

    /**
     * Remove all the readings taken on the given day.
     * @param day The day to be removed.
     */
    public void removeReadingsOnDay(int day)
    {
        Iterator<Reading> it = readings.iterator();
        while(it.hasNext())
        {
            Reading readings = it.next();
            int details = readings.getDay(); //storing the day of the readings in local variable 
                                             //called "details"
            if(details == day) 
            {
                it.remove(); //if the variable above is true, remove that day
            }
        }
    }

    /**
     * Find the earliest day on which the first recorded temperature
     * was above the given ceiling.
     * If there are no readings at all, return -1.
     * If there is no temperature above the ceiling, return the
     * earliest day on which the highest temperature was recorded.
     * @param ceiling Look for a temperature above this value.
     * @return the matching day, or -1 if there are no readings.
     */
    public int findFirstAbove(double ceiling)
    {
        Iterator<Reading> it = readings.iterator();
        int value = 0;
        int day = 0;
        boolean valueFound = false;
        while(it.hasNext())
        {
            Reading readings = it.next();
            if(readings.getTemperature() > ceiling && valueFound == false)
            {
                value = readings.getDay(); 
                valueFound = true;
            }
        }
        if(valueFound == false)
        {
            double highestTemp = -100;
            for(Reading readings : readings)
            {
                if(readings.getTemperature() > highestTemp)
                {
                    highestTemp = readings.getTemperature();
                    day = readings.getDay();
                }
            }
            value = day;
        }
        if(readings.isEmpty())  //if there are no readings 
        {
            value = -1;
        }
        return value;
    } 
}

/**
 * Model a temperature reading from a sensor.
 * @author David J. Barnes (d.j.barnes@kent.ac.uk)
 * @version 2016.11.02
 */
public class Reading
{
    private final int location;
    private final int day;
    private final double temperature;

    /**
     * Create a Reading object.
     * @param location The location of the reading.
     * @param day The day of the reading.
     * @param temperature The temperature in the location.
     */
    public Reading(int location, int day, double temperature)
    {
        this.location = location;
        this.day = day;
        this.temperature = temperature;
    }

    /**
     * Get the location.
     * @return the location.
     */
    public int getLocation()
    {
        return location;
    }

    /**
     * Get the day.
     * @return the day.
     */
    public int getDay()
    {
        return day;
    }

    /**
     * Get the temperature.
     * @return the temperature.
     */
    public double getTemperature()
    {
        return temperature;
    }
    
    /**
     * Get the formatted details.
     * @return the formatted details.
     */
    public String getDetails()
    {
        
        return String.format("Day: %d, Location: %d, Temperature: %.2f", day, location, temperature);
    }
    
}

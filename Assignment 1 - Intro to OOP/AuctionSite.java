
/**
 * @author (Nun Srijae) 
 * @version (a version number or a date)
 */
public class AuctionSite
{
    //Field type integer to store numbers eg. amount of the highest bid, new bid and the increment
    private int highestbid,newbid,increment;
    // Strings to store description of the products 
    private String description; String nameofitem;
    
    public AuctionSite(String name, String description)
    {
        nameofitem = name;
        this.description = description;
        newbid = 0;
        this.increment = highestbid+(highestbid*10)/100+1; //doing the sum of 10 per cent, plus 1
    }

    public String getDescription()
    {
        return this.description;
    }  
     /**
      * return the value of the highest bid
      */
    public int getHighestBid()
    {
     return this.highestbid;    
    }
    
    /**
     * Preventing customers from making the same amount of bid and/or an ammount that is only a pence or two difference
     */
    public void setBid(int newbid)
    {   
        this.newbid = newbid;
        if(newbid < increment)
        {
            System.out.println("Please bid at least: " + increment); 
        }
        else 
        /**
         * if new bid is higher than the current highest bid then a ""Happy bidding" message will appear
         */
        {
            highestbid = newbid;
            System.out.println("Happy bidding!");
            this.increment = highestbid+(highestbid*10)/100+1; //doing the sum of 10 per cent, plus 1
        }
       
    }
  
    }
        
  


   
    
   
    


 
    
    
    
    
 
   



